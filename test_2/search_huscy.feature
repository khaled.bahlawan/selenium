Feature: Huscy search

    Scenario: Search for huscy in PyPI
        Given I'm on the PyPI homepage
        When I enter search term as huscy
        Then search result for huscy should appear
