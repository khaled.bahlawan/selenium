from pprint import pprint

#lst = ["huscy.se", "huscy_qweq", "qweqw.huscy", "huscy"]
lst = ['huscy.pseudonyms 1.0.1\nJan 23, 2021\nManaging pseudonyms for subjects.',
 'huscy.projects 0.1.0a8\nJul 17, 2020',
 'huscy.subjects 1.0.2\n'
 'Feb 15, 2021\n'
 'Managing subjects in a human research context.',
 'huscy.users 0.1.0a1\nJul 27, 2020',
 'huscy.recruitment 0.5.3a19\nFeb 1, 2021',
 'huscy.rooms 0.1.0a3\nMar 23, 2020',
 'huscy.appointments 1.1.1\nMay 6, 2020',
 'huscy.bookings 0.1.0a2\nNov 6, 2020',
 'huscy.attributes 0.3.0a5\nSep 2, 2020',
 'huscy.data-request 0.1.0a0\nJul 24, 2020',
 'huscy.project-memberships 0.3.0a2\nApr 14, 2020',
 'huscy.project-archivenotes 0.1.0a0\nMar 18, 2020',
 'huscy.project-ethics 0.2.0a1\nApr 8, 2020',
 'huscy.project-storage 0.1.0a0\nApr 14, 2020',
 'huscy.project-documents 0.5.0b4\nFeb 9, 2021']



def count_huscies(my_list):
    return len([x for x in lst if x.startswith("huscy")])


#pprint(count_huscies(lst))
pprint(count_huscies(lst))
