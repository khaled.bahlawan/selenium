from pytest_bdd import scenario, given, when, then
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

import pytest


DELAY = 3  # Seconds


@pytest.fixture
def driver():
    driver = webdriver.Firefox()
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


@scenario('search_huscy.feature', 'Search for huscy in PyPI')
def test_search():
    pass


@given("I'm on the PyPI homepage")
def go_to_pypi(driver):
    url = "https://pypi.org/"
    driver.get(url)


@when("I enter search term as huscy")
def search_for_huscy(driver):
    inputElement = driver.find_element_by_id("search")
    inputElement.send_keys("huscy" + Keys.RETURN)
    wait_until_text_shown(driver, "content", 'projects for "huscy"')
    #inputElement.send_keys(Keys.ENTER)
    #inputElement.submit()


@then("search result for huscy should appear")
def find_huscy(driver):
    searchElements = driver.find_elements_by_class_name("package-snippet")
    assert count_huscies(searchElements) == 15

    searchElements = driver.find_elements_by_partial_link_text("huscy")
    assert len(searchElements) == 15


def count_huscies(lst):
    return len([x for x in lst if x.text.startswith("huscy")])


def wait_until_text_shown(driver, element_ID, searched_text):
    try:
        element_present = EC.text_to_be_present_in_element((By.ID, element_ID), searched_text)
        WebDriverWait(driver, DELAY).until(element_present)
        print("\n<<Page is ready>>")
    except TimeoutException:
        print("Loading took too much time!")
