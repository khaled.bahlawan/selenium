from pytest_bdd import scenario, given, when, then
import pytest
import selenium
from splinter.browser import Browser


@pytest.fixture
def browser():
    browser = Browser()
    return browser


@scenario('publish_article.feature', 'Publishing the article')
def test_publish():
    pass


@given("I'm a website")
def check_website():
    assert True
    #browser.visit(urljoin(browser.url, 'https://pypi.org/'))


@when("I go to the article")
def go_to_the_article(browser):
    #assert True
    url = "https://pypi.org/"
    browser.visit(url)
    #selenium.get('https://pypi.org/')
    #browser.visit(urljoin(browser.url, ""))


@then("Yes")
def return_true():
    assert True
